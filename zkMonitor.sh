#!/bin/bash

export STAT_FILE=/tmp/stat_file.tmp
export SRVR_FILE=/tmp/srvr_file.tmp
export CONF_FILE=/tmp/conf_file.tmp

echo stat | nc 127.0.0.1 2181 > $STAT_FILE
echo srvr | nc 127.0.0.1 2181 > $SRVR_FILE
echo conf | nc 127.0.0.1 2181 > $CONF_FILE

function print_header {
	echo ""
	echo "=== $1 ==="
}

# Server Status
print_header "Server Status"
ROUK=$(echo ruok | nc 127.0.0.1 2181)
if [ $? -eq 0 ]; then
	if [ $ROUK != "imok" ]; then
		echo "Server Status: $ROUK"
	else
		echo "Server Status OK"
	fi	
else
	echo "Server not responding..."
fi

# Server ID
print_header "Server ID"
SID=$(cat $CONF_FILE | grep serverId)
echo $SID

# Server Mode
print_header "Server Mode"

ISRO=$(echo isro | nc 127.0.0.1 2181)
if [ $ISRO == "ro" ]; then
	echo "Server is in Read Only mode"
else
	if [ $ISRO == "rw" ]; then
        	echo "Server is in Read Write mode"
	else
		echo "Server is in $ISRO mode"
	fi
fi
MODE=$(cat $STAT_FILE | grep Mode)
echo $MODE

# Messages
print_header "Messages"

RCVD=$(cat $SRVR_FILE | grep Received)
SENT=$(cat $SRVR_FILE | grep Sent)
echo $RCVD
echo $SENT

# Connections
print_header "Current Connections"

CONN=$(cat $SRVR_FILE | grep Connections)
echo $CONN

# Latency
print_header  "Latencies"
LAT=$(cat $SRVR_FILE | grep Latency)
echo $LAT

# Clients
print_header "Connected Clients"
# echo cons | nc 127.0.0.1 2181
CLIENTS=$(sed -n '/Clients/,/^$/{/./p}' $STAT_FILE)
echo $CLIENTS

# Data Dirs
print_header "Data Directories"
DATADIR=$(cat $CONF_FILE | grep dataDir)
DATALOGDIR=$(cat $CONF_FILE | grep dataLogDir)
echo $DATADIR
echo $DATALOGDIR



echo ""
